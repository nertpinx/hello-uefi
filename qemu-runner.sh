#!/bin/sh

set -e

main() {
    # Clear the screen colours on exit
    trap 'echo -e "\e[0;0m"' EXIT

    local OVMF_PATH
    local SUBPATH
    local IMAGE_NAME="disk.img"
    local EFI_PATH="$1"
    local CMD

    if test -z "$EFI_PATH"
    then
        echo "No path to EFI binary, are you using 'cargo run'?" >&2
        return 1
    fi

    for CMD in qemu-system-x86_64
    do
        if ! type "$CMD" >/dev/null 2>&1
        then
            echo "Could not find command '$CMD', aborting" >&2
            return 1
        fi
    done

    for SUBPATH in edk2/ovmf OVMF edk2-ovmf
    do
        local TEST_PATH="/usr/share/$SUBPATH/OVMF_CODE.fd"
        if test -r "$TEST_PATH"
        then
           OVMF_PATH="$TEST_PATH"
           break
        fi
    done

    if test -z "$OVMF_PATH"
    then
        echo "Could not find accessible OVMF_CODE.fd" >&2
        return 1
    fi

    "$(dirname "$0")/build_image.sh" "$IMAGE_NAME" "$EFI_PATH"

    # Of course you should use libvirt to make sure it searches for the proper
    # firmware based on the JSON descriptors, takes care of copying the OVMF vars
    # and making sure the code is read-only and million of other things.
    qemu-system-x86_64 \
        -bios "$OVMF_PATH" \
        -display gtk \
        -serial stdio \
        -drive file="$IMAGE_NAME",format=raw
}

main "$@"
