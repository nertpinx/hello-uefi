#!/bin/sh

set -e

main() {
    local IMAGE_NAME="$1"
    local EFI_PATH="$2"
    local EFI_NAME="$(basename $2)"

    local CMD

    for CMD in stat sfdisk guestfish
    do
        if ! type "$CMD" >/dev/null 2>&1
        then
            echo "Could not find command '$CMD', aborting" >&2
            return 1
        fi
    done

    if test -e "$IMAGE_NAME"
    then
        if test "$(stat -c %Y "$IMAGE_NAME")" -gt "$(stat -c %Y "$EFI_PATH")"
        then
            # Yes, we sometimes won't rebuild after switch from/to --release
            # build, but it's better than rebuilding the image from scratch
            # every time and to be frank, who really cares since nobody will
            # ever use this apart from me.
            return 0
        fi
        truncate -s0 "$IMAGE_NAME"
    fi

    local FILESIZE
    FILESIZE="$(stat -c %s "$EFI_PATH")"
    FILESIZE="$((FILESIZE*2))"

    truncate -s "$FILESIZE" "$IMAGE_NAME"

    sfdisk "$IMAGE_NAME" <<EOF
label: dos
- - U *
EOF

    guestfish --rw -a "$IMAGE_NAME" <<EOF
run
mkfs fat /dev/sda1
mount /dev/sda1 /
mkdir /efi
mkdir /efi/boot
copy-in "$EFI_PATH" /efi/boot
mv /efi/boot/$EFI_NAME /efi/boot/bootx64.efi
EOF
}

main "$@"
