# Hello world in Rust for UEFI

This is just a toy project that uses
[uefi-rs](https://github.com/rust-osdev/uefi-rs) to write a simple EFI
executable written in [Rust](https://www.rust-lang.org).

Second part of this project is creating a bootable image and running it under
[QEMU](https://www.qemu.org).  This is done in a hopefully-POSIX shell script
which is registered in `.cargo/config.toml` as the runner for the executables in
the project.

This should by no means be taken as a clean code or looked at for too much of an inspiration, I mostly did not know what I was doing.

You can try it out yourself if you have all the necessary tools installed, just run:

```
cargo run [--release]
```

and it should build the executable, create a disk image and run QEMU with it.
The disk image is created using `guestfish` from
[libguestfs](https://libguestfs.org) as it does not require root access and
extra capabilities compared to using e.g. `losetup`.  You can use that disk
image to flash it to a drive using:

```
dd if=disk.img of=/path/do/device oflag=fsync
```

Although I have no idea why anyone would do that =)

Prebuilt image is available as an artefact of the CI, link to the latest one is
here:

https://gitlab.com/nertpinx/hello-uefi/-/jobs/artifacts/master/raw/disk.img?job=build

It can be run in a UEFI-enabled VM, one of the ways to do that is:

```
virt-install -n $VM_NAME --boot uefi --memory 32 --import --disk /path/to/disk.img --os-variant unknown [ --autoconsole text ]
```

or if you have new enough virt-manager you can run it directly from the artifact with:

```
virt-install -n $VM_NAME --memory 32 --boot uefi --import --disk path='https://gitlab.com/nertpinx/hello-uefi/-/jobs/artifacts/master/raw/disk.img?job=build',readonly=true,target.bus=virtio --os-variant unknown [ --autoconsole text ]
```

I hope you have fun.

## TODO

* Figure out how to properly pass it to QEMU, even as read-only directly from the URL, curl driver does work, but qemu ends with `qemu-system-x86_64: Block node is read-only`
