#![no_std]
#![no_main]
#![feature(abi_efiapi)]

use core::fmt::Write;

use uefi::prelude::*;
use uefi::proto::console::text::*;
use uefi::ResultExt;

extern crate alloc;
use alloc::string::String;

const PROMPT: &str = "What's your name: ";

#[entry]
fn main(_handle: Handle, mut system_table: SystemTable<Boot>) -> Status {
    uefi_services::init(&mut system_table).unwrap_success();

    let stdout = system_table.stdout();

    stdout.reset(true).unwrap().log();
    stdout.set_color(Color::Green, Color::Black).unwrap().log();

    log::warn!("I have no idea what I'm doing");

    write!(system_table.stdout(), "{}", PROMPT).unwrap();

    let mut read_completed = false;
    let mut name = String::new();
    let mut events = [unsafe { system_table.stdin().wait_for_key_event().unsafe_clone() }];
    while !read_completed {
        system_table
            .boot_services()
            .wait_for_event(&mut events)
            .unwrap_success();

        while let Some(key) = system_table.stdin().read_key().unwrap_success() {
            match key {
                Key::Printable(c16) => {
                    let c = char::from(c16);

                    if c == '\r' {
                        read_completed = true;
                        writeln!(system_table.stdout()).unwrap();
                        break;
                    }

                    name.push(c);
                    write!(system_table.stdout(), "{}", c).unwrap();
                }
                Key::Special(ScanCode::DELETE) => {
                    name.pop();
                    write!(
                        system_table.stdout(),
                        "\r{}{} \r{}{}",
                        PROMPT,
                        name,
                        PROMPT,
                        name
                    )
                    .unwrap();
                }
                _ => (),
            };
        }
    }

    writeln!(
        system_table.stdout(),
        "Yes, this is just a stupid hello world app, {}!",
        name
    )
    .unwrap();

    writeln!(system_table.stdout(), "Press any key to shut down").unwrap();

    let mut events = [unsafe { system_table.stdin().wait_for_key_event().unsafe_clone() }];
    system_table
        .boot_services()
        .wait_for_event(&mut events)
        .unwrap_success();

    system_table.runtime_services().reset(
        uefi::table::runtime::ResetType::Shutdown,
        Status::SUCCESS,
        None,
    );
}
